#include "salida.h"
#include <conio.h>
#include <string.h>
#include "liblib.h"

void barra()
{
	int i;
	textbackground(WHITE);
	clrscr();
	textcolor(BLACK);
	for (i=1; i<=52; i++)
	{
		gotoxy(14+i, 11);
		cprintf("x");
		gotoxy(14+i, 13);
		cprintf("x");
	}

	gotoxy(15, 12);
	cprintf("x");
	gotoxy(66, 12);
	cprintf("x");
	for (i=1; i<=50; i++)
	{
		textbackground(GREEN);
		gotoxy(15+i, 12);
		cprintf(" ");
		textbackground(WHITE);
		delay(150);
		textcolor(RED);
		gotoxy(39, 10);
		cprintf("%d %c", i*2, '%');
	}
	gotoxy(35, 14);
	textcolor(RED + BLINK);
	cprintf("Carga completa!");
	getch();
	librerias();
}

void acceso()
{
	int i, k;
	char pass[15], x;

	for (k=1; k<=3; k++)
	{
		textbackground(BLUE);
		clrscr();
		textbackground(RED);
		textcolor(WHITE);

		for (i=1; i<=40; i++)
		{
			gotoxy(19+i, 12);
			cprintf(" ");

		}
		for (i=1; i<=42; i++)
		{
			gotoxy(18+i, 11);
			cprintf("x");
			gotoxy(18+i, 13);
			cprintf("x");
		}

		gotoxy(19, 12);
		cprintf("x");
		gotoxy(60, 12);
		cprintf("x");

		textbackground(BLUE);
		textcolor(WHITE);
		gotoxy(35, 10);
		cprintf("Intento %d", k);
		gotoxy(21, 12);
		printf("Ingrese contrase%ca: ", 164);
		i=0;
		for (; ;)
		{
			x=getch();
			textbackground(RED);
			if (x!='\r'&&x!='\b')
			{
				putch('*');
				pass[i]=x;
				i++;
			}
			else if (x=='\b'&&i>0)
			{
				i--;
				putch(x);
				putch(' ');
				putch(x);
			}
			else if (x=='\r')
				break;
			while (i==11)
			{
				i--;
				putch(8);
				putch(' ');
				putch(8);
			}
		}
		pass[i]='\0';
		textbackground(BLUE);
		if ((strcmp(pass, "Thrashans"))==0)
		{
			gotoxy(30, 14);
			cprintf("Contrase%ca correcta", 164);
			getch();
			barra();
			break;
		}
		else
		{
			if (k==3)
			{
				gotoxy(30, 14);
				cprintf("Contrase%ca incorrecta", 164);
				gotoxy(27, 15);
				cputs("Han pasado los tres intentos");
				getch();
				salir();
			}
			else
			{
				gotoxy(30, 14);
				cprintf("Contrase%ca incorrecta", 164);
				getch();
			}
		}
	}
}

void seguir()
{
	char x;
	textcolor(RED);
	gotoxy(58, 24);
	cprintf("%cDesea continuar? (S/N)", 168);
	x=getch();
	switch(x)
	{
		case 'S':
		case 's':
			acceso();
			break;
		case 'N':
		case 'n':
			salir();
			break;
		default:
			seguir();
	}
}
