#include <conio.h>
#include <dos.h>
#include <stdlib.h>
#define tiempo 600

void salir()
{
	int i;
	textbackground(BLACK);
	clrscr();
	textcolor(WHITE);
	for (i=1; i<=25; i++)
	{
		clrscr();
		if (i<=18)
		{
			gotoxy(33, 19-i);
			printf("Aprendizajes obtenidos:");
		}
		if (i<=20)
		{
			gotoxy(35, 21-i);
			printf("-Uso de ciclos");
		}
		if (i<=21)
		{
			gotoxy(35, 22-i);
			printf("-Arreglos");
		}
		if (i<=22)
		{
			gotoxy(35, 23-i);
			printf("-Cadena de caracteres");
		}
		if (i<=23)
		{
			gotoxy(35, 24-i);
			printf("-Funciones");
		}
		if (i<=24)
		{
			gotoxy(35, 25-i);
			printf("-Creacion de librerias");
		}
		delay(tiempo);
	}
	exit(0);
}
